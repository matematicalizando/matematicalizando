<?php 

class Funcoes 
{
    public function MontaModal($tipo)
    { ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
            <div class="form-example-int mg-t-15">
                <button type="submit" class="btn btn-info" data-toggle="modal" data-target="#resultado<?php echo $tipo; ?>" id="botao-calcular-<?php echo $tipo ?>">Calcular</button>
            </div>
        </div>
        <div class="modal animated fadeInDown" id="resultado<?php echo $tipo; ?>" role="dialog">
            <div class="modal-dialog modals-default">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 align="center">Resultado</h4>
                        <hr>
                    </div>
                    <div class="modal-body" id="conteudo-<?php echo $tipo; ?>">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
        </div>
    <?php } 

    public function MontaInputNumPositivo($nome, $id)
    { ?>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-example-int">
                <div class="form-group">
                    <label><?php echo $nome; ?></label>
                    <div class="nk-int-st">
                        <input id="<?php echo $id; ?>" name="<?php echo $id; ?>" placeholder="Insira um valor" class="verifica_numerico form-control input-sm" required type="text" maxlength="3" size="4">
                    </div>
                </div>
            </div>
        </div>
    <?php }

    public function MontaCabecalho($titulo, $imagem, $largura, $altura)
    { ?>
        <h2 align="center"><?php echo $titulo; ?> <span><img src="../images/<?php echo $imagem; ?>" width="<?php echo $largura; ?>px" height="<?php echo $altura; ?>px" style="margin-bottom: 8px;"></span></h2>
    <?php }
}

?>