<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Paralelepípedo', 'paralelipipedo.png', 50, 50);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                        Paralelepípedo ou bloco retangular é a designação dada a um prisma cujas faces são paralelogramos. Um paralelepípedo tem seis faces, sendo que duas são idênticas e paralelas entre si. Os paralelepípedos podem ser retos ou oblíquos, consoante as suas faces laterais sejam perpendiculares ou não à base. O paralelepípedo possui 12 arestas e 8 vértices. <span><a href="https://pt.wikipedia.org/wiki/Paralelep%C3%ADpedo" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#volume">Volume</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="volume" class="tab-pane fade in active">
                            <form id="form-volume">
                                <div class="tab-ctn">
                                    <p>O volume do paralelepípedo é dado pela fórmula: <strong>V = c * l * h</strong>.</p>
                                    <p>Legenda: <strong>V = </strong> Volume, <strong>c = </strong>Comprimento, <strong>l = </strong>Largura, <strong>h = Altura</strong></p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Comprimento', 'comprimento');
                                            $funcoes->MontaInputNumPositivo('Largura', 'largura');
                                            $funcoes->MontaInputNumPositivo('Altura', 'altura'); 
                                            $funcoes->MontaModal('volume');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularVolume = document.querySelector("#botao-calcular-volume");
    botaoCalcularVolume.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-volume");
        var modal = document.querySelector("#conteudo-volume");

        document.getElementById("conteudo-volume").innerHTML = "";

        var comprimento = parseFloat(form.comprimento.value);
        var largura = parseFloat(form.largura.value);
        var altura = parseFloat(form.altura.value);

        var resultado = document.createElement("p");

        resultado.appendChild(volumeParalelepipedo(comprimento, largura, altura));

        modal.appendChild(resultado);
    });

</script>