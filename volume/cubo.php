<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Cubo', 'cubo.png', 36, 36);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                        Um cubo ou hexaedro regular é um poliedro com 6 faces congruentes. Além disso, é um dos cinco sólidos platônicos. <span><a href="https://pt.wikipedia.org/wiki/Cubo" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#volume">Volume</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="volume" class="tab-pane fade in active">
                            <form id="form-volume">
                                <div class="tab-ctn">
                                    <p>O volume do cubo é dado pela fórmula: <strong>V = l³</strong>.</p>
                                    <p>Legenda: <strong>V = </strong> Volume, <strong>l = </strong> Lado</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Lado', 'lado'); 
                                            $funcoes->MontaModal('volume');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularVolume = document.querySelector("#botao-calcular-volume");
    botaoCalcularVolume.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-volume");
        var modal = document.querySelector("#conteudo-volume");

        document.getElementById("conteudo-volume").innerHTML = "";

        var lado = parseFloat(form.lado.value);

        var resultado = document.createElement("p");

        resultado.appendChild(volumeCubo(lado));

        modal.appendChild(resultado);
    });

</script>