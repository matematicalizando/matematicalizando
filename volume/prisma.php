<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Prisma', 'prisma.png', 36, 36);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                        Um prisma é o sólido geométrico formado pela união de todos os segmentos de reta congruentes e paralelos a um segmento dado, com uma extremidade nos pontos de um polígono fixo não paralelo a esse. Ou seja, um prisma é um poliedro com duas faces congruentes e paralelas (bases) e cujas demais faces (faces laterais) são paralelogramos. <span><a href="https://pt.wikipedia.org/wiki/Prisma" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#prisma">Prisma</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="volume" class="tab-pane fade in active">
                            <form id="form-volume">
                                <div class="tab-ctn">
                                    <p>O volume do prisma é dado pela fórmula: <strong>V = Ab * h</strong>.</p>
                                    <p>Legenda: <strong>V = </strong> Volume, <strong>Ab = </strong> Área da base, <strong>h = </strong> Altura</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Área base', 'areab');
                                            $funcoes->MontaInputNumPositivo('Altura', 'altura'); 
                                            $funcoes->MontaModal('volume');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularVolume = document.querySelector("#botao-calcular-volume");
    botaoCalcularVolume.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-volume");
        var modal = document.querySelector("#conteudo-volume");

        document.getElementById("conteudo-volume").innerHTML = "";

        var areab = parseFloat(form.areab.value);
        var altura = parseFloat(form.altura.value);

        var resultado = document.createElement("p");

        resultado.appendChild(volumePrisma(areab, altura));

        modal.appendChild(resultado);
    });

</script>