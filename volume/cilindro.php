<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Cilindro', 'cilindro.png', 50, 50);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                        Em Geometria, um cilindro é o objeto tridimensional delimitado pela superfície de translação completa de um segmento de reta que se move paralelamente a si mesmo, e se apoia em uma circunferência. De maneira mais prática, o cilindro é um corpo alongado e de aspecto redondo, com o mesmo diâmetro ao longo de todo o comprimento. <span><a href="https://pt.wikipedia.org/wiki/Cilindro" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#volume">Volume</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="volume" class="tab-pane fade in active">
                            <form id="form-volume">
                                <div class="tab-ctn">
                                    <p>O volume do Cilindro é dado pela fórmula: <strong>V = πr² * h</strong>.</p>
                                    <p>Legenda: <strong>V = </strong> Volume, <strong>π = </strong> Pi, <strong>r = </strong>Raio, <strong>h = Altura</strong></p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Raio', 'raio');
                                            $funcoes->MontaInputNumPositivo('Altura', 'altura'); 
                                            $funcoes->MontaModal('volume');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularVolume = document.querySelector("#botao-calcular-volume");
    botaoCalcularVolume.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-volume");
        var modal = document.querySelector("#conteudo-volume");

        document.getElementById("conteudo-volume").innerHTML = "";

        var raio = parseFloat(form.raio.value);
        var altura = parseFloat(form.altura.value);

        var resultado = document.createElement("p");

        resultado.appendChild(volumeCilindro(raio, altura));

        modal.appendChild(resultado);
    });

</script>