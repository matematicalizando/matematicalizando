/* --- ÁREA E PERÍMETRO --- */

/* QUADRADO */
function areaQuadrado(lado) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Área = '+lado+'²';

    var l2 = document.createElement("li");
    l2.textContent = 'Área = '+(lado * lado);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);      
    resultado.appendChild(negrito);

    return resultado; 
}

function perimetroQuadrado(lado) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Perímetro = 4 * '+lado;

    var l2 = document.createElement("li");
    l2.textContent = 'Perímetro = '+(lado * 4);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);      
    resultado.appendChild(negrito);

    return resultado; 
}

/* RETÂNGULO */
function areaRetangulo(base, altura) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Área = '+base+' * '+altura;

    var l2 = document.createElement("li");
    l2.textContent = 'Área = '+base * altura;

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);      
    resultado.appendChild(negrito);

    return resultado; 
}

function perimetroRetangulo(base, altura) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Perímetro = 2 * ('+base+' + '+altura+')';

    var l2 = document.createElement("li");
    l2.textContent = 'Perímetro = 2 * ('+(base+altura)+')';

    var l3 = document.createElement("li");
    l3.textContent = 'Perímetro = '+2*(base+altura);

    var negrito = document.createElement("strong");
    negrito.appendChild(l3);

    resultado.appendChild(l1);
    resultado.appendChild(l2);      
    resultado.appendChild(negrito);

    return resultado; 
}

/* TRIÂNGULO */
function areaTriangulo(base, altura) {

        var resultado = document.createElement("ul");

        var l1 = document.createElement("li");      
        l1.textContent = 'Área = ('+base+' * '+altura + ') / 2';

        var l2 = document.createElement("li");
        l2.textContent = 'Área = '+(base * altura) + ' / 2';

        var l3 = document.createElement("li");
        l3.textContent = 'Área = '+(base * altura) / 2;

        var negrito = document.createElement("strong");
        negrito.appendChild(l3);

        resultado.appendChild(l1);
        resultado.appendChild(l2);      
        resultado.appendChild(negrito);

        return resultado; 
}

function perimetroTriangulo(lado1, lado2, lado3) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Perímetro = '+lado1+' + '+lado2+' + '+lado3;

    var l2 = document.createElement("li");
    l2.textContent = 'Perímetro = ' + (lado1 + lado2 + lado3);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);  
    resultado.appendChild(negrito);

    return resultado; 
}

/* LOSANGO */
function areaLosango(diagonalM, diagonalm) {

        var resultado = document.createElement("ul");

        var l1 = document.createElement("li");      
        l1.textContent = 'Área = ('+diagonalM+' * '+diagonalm + ') / 2';

        var l2 = document.createElement("li");
        l2.textContent = 'Área = '+(diagonalM * diagonalm) + ' / 2';

        var l3 = document.createElement("li");
        l3.textContent = 'Área = '+(diagonalM * diagonalm) / 2;

        var negrito = document.createElement("strong");
        negrito.appendChild(l3);

        resultado.appendChild(l1);
        resultado.appendChild(l2);      
        resultado.appendChild(negrito);

        return resultado; 
}

function perimetroLosango(lado) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Perímetro = '+lado+' * 4';

    var l2 = document.createElement("li");
    l2.textContent = 'Perímetro = ' + (lado * 4);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);  
    resultado.appendChild(negrito);

    return resultado; 
}

/* TRAPÉZIO */
function areaTrapezio(baseM, basem, altura) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Área = (('+baseM+' * '+basem + ') / 2) * '+altura;

    var l2 = document.createElement("li");
    l2.textContent = 'Área = ('+(baseM * basem) + ' / 2) * '+altura;

    var l3 = document.createElement("li");
    l3.textContent = 'Área = '+(baseM * basem) / 2 + ' * '+altura;

     var l4 = document.createElement("li");
    l4.textContent = 'Área = '+((baseM * basem) / 2) * altura;

    var negrito = document.createElement("strong");
    negrito.appendChild(l4);

    resultado.appendChild(l1);
    resultado.appendChild(l2);
    resultado.appendChild(l3);         
    resultado.appendChild(negrito);

    return resultado; 
}

function perimetroTrapezio(baseM, basem, lado1, lado2) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Perímetro = '+baseM+' + '+basem+' + '+lado1+' + '+lado2;

    var l2 = document.createElement("li");
    l2.textContent = 'Perímetro = ' + (baseM + basem + lado1 + lado2);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);  
    resultado.appendChild(negrito);

    return resultado; 
}

/* POLIGONO */
function areaPoligono(perimetro, apotema) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Área = ('+perimetro+' / 2) * '+apotema;

    var l2 = document.createElement("li");
    l2.textContent = 'Área =  '+perimetro / 2+' * '+apotema;

    var l3 = document.createElement("li");
    l3.textContent = 'Área = '+(perimetro / 2) * apotema;

    var negrito = document.createElement("strong");
    negrito.appendChild(l3);

    resultado.appendChild(l1);
    resultado.appendChild(l2);        
    resultado.appendChild(negrito);

    return resultado; 
}

function perimetroPoligono(lado, numLados) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Perímetro = '+lado+' * '+numLados;

    var l2 = document.createElement("li");
    l2.textContent = 'Perímetro =  '+(lado * numLados);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);      
    resultado.appendChild(negrito);

    return resultado; 
}

/* CÍRCULO */
function areaCirculo(raio) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Área = π * '+raio+'²';

    var l2 = document.createElement("li");
    l2.textContent = 'Área =  3,14 * '+(raio * raio);

    var l3 = document.createElement("li");
    l3.textContent = 'Área = '+(3.14 * (raio * raio));

    var negrito = document.createElement("strong");
    negrito.appendChild(l3);

    resultado.appendChild(l1);
    resultado.appendChild(l2);        
    resultado.appendChild(negrito);

    return resultado; 
}

function perimetroCirculo(raio) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Área = 2 * π * '+raio;

    var l2 = document.createElement("li");
    l2.textContent = 'Área = 2 * 3,14 * '+raio;

    var l3 = document.createElement("li");
    l3.textContent = 'Área =  '+(2 * 3.14)+' * '+raio;

    var l4 = document.createElement("li");
    l4.textContent = 'Área = '+(2 * 3.14 * raio);

    var negrito = document.createElement("strong");
    negrito.appendChild(l4);

    resultado.appendChild(l1);
    resultado.appendChild(l2);   
    resultado.appendChild(l3);     
    resultado.appendChild(negrito);

    return resultado; 
}