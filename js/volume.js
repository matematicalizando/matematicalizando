/* --- VOLUME --- */

// CUBO
function volumeCubo(lado) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Volume = '+lado+'³';

    var l2 = document.createElement("li");
    l2.textContent = 'Volume = '+(lado * lado * lado);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);      
    resultado.appendChild(negrito);

    return resultado; 
}

// PARALELEPÍPEDO
function volumeParalelepipedo(comprimento, largura, altura) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Volume = '+comprimento+' * '+largura+' * '+altura;

    var l2 = document.createElement("li");
    l2.textContent = 'Volume = '+(comprimento * largura)+' * '+altura;

    var l3 = document.createElement("li");
    l3.textContent = 'Volume = '+(comprimento * largura * altura);

    var negrito = document.createElement("strong");
    negrito.appendChild(l3);

    resultado.appendChild(l1);
    resultado.appendChild(l2);      
    resultado.appendChild(negrito);

    return resultado; 
}

// PRISMA
function volumePrisma(areab, altura) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Volume = '+areab+' * '+altura;

    var l2 = document.createElement("li");
    l2.textContent = 'Volume = '+(areab * altura);

    var negrito = document.createElement("strong");
    negrito.appendChild(l2);

    resultado.appendChild(l1);  
    resultado.appendChild(negrito);

    return resultado; 
}

// PRISMA
function volumeCilindro(raio, altura) {

    var resultado = document.createElement("ul");

    var l1 = document.createElement("li");      
    l1.textContent = 'Volume = π * '+raio+'² * '+altura;

    var l2 = document.createElement("li");
    l2.textContent = 'Volume = 3,14 * '+(raio * raio)+' * '+altura;

    var l3 = document.createElement("li");
    l3.textContent = 'Volume = '+3.14 * raio * raio +' * '+altura;

    var l4 = document.createElement("li");
    l4.textContent = 'Volume = '+3.14 * raio * raio * altura;

    var negrito = document.createElement("strong");
    negrito.appendChild(l4);

    resultado.appendChild(l1);
    resultado.appendChild(l2);
    resultado.appendChild(l3); 
    resultado.appendChild(negrito);

    return resultado; 
}