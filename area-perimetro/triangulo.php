<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Triângulo', 'triangulo.png', 36, 36);
?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="widget-tabs-int">
				<div class="tab-hd">
					<p>
						No plano, o triângulo é a figura geométrica que ocupa o espaço interno limitado por três segmentos de reta que concorrem, dois a dois, em três pontos diferentes formando três lados e três ângulos internos que somam 180°. Também se pode definir um triângulo em superfícies gerais. Nesse caso, são chamados de triângulos geodésicos e têm propriedades diferentes. Também podemos dizer que o triângulo é a união de três pontos não-colineares (pertencente a um plano, em decorrência da definição dos mesmos), por três segmentos de reta. <span><a href="https://pt.wikipedia.org/wiki/Tri%C3%A2ngulo" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
					</p>
				</div>
				<div class="widget-tabs-list">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#area">Área</a></li>
						<li><a data-toggle="tab" href="#perimetro">Perímetro</a></li>
					</ul>									
					<div class="tab-content tab-custom-st">
						<div id="area" class="tab-pane fade in active">
							<form id="form-area">
								<div class="tab-ctn">
									<p>A área do triângulo é dada pela fórmula: <strong>A = (b * a) / 2</strong>.</p>
									<p>Legenda: <strong>A = </strong> Área, <strong>a = </strong> Altura, <strong>b = </strong>Base</p>
									<hr>
									<p class="tab-mg-b-0">
										<div align="center"><h4>Calcular</h4></div>
										<?php 
											$funcoes->MontaInputNumPositivo('Base', 'base'); 
											$funcoes->MontaInputNumPositivo('Altura', 'altura');
											$funcoes->MontaModal('area');
										?>
									</p>
								</div>
							</form>
						</div>

						<div id="perimetro" class="tab-pane fade">
							<form id="form-perimetro">
								<div class="tab-ctn">
									<p>O perímetro do Triangulo é dado pela fórmula: <strong>P = lado1 + lado2 + lado3</strong>.</p>
									<p>Legenda: <strong>P = </strong> Perímetro</p>
									<hr>
									<p class="tab-mg-b-0">
										<div align="center"><h4>Calcular</h4></div>
										<?php 
											$funcoes->MontaInputNumPositivo('Lado1', 'lado1'); 
											$funcoes->MontaInputNumPositivo('Lado2', 'lado2');
											$funcoes->MontaInputNumPositivo('Lado3', 'lado3');
											$funcoes->MontaModal('perimetro');
										?>
									</p>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

	var botaoCalcularArea = document.querySelector("#botao-calcular-area");
	botaoCalcularArea.addEventListener("click", function(event){

		event.preventDefault();

		var form = document.querySelector("#form-area");
		var modal = document.querySelector("#conteudo-area");

		document.getElementById("conteudo-area").innerHTML = "";

		var base = parseFloat(form.base.value);
		var altura = parseFloat(form.altura.value);

		var resultado = document.createElement("p");

		resultado.appendChild(areaTriangulo(base, altura));

		modal.appendChild(resultado);
	});

	var botaoCalcularPerimetro = document.querySelector("#botao-calcular-perimetro");
	botaoCalcularPerimetro.addEventListener("click", function(event){

		event.preventDefault();

		var form = document.querySelector("#form-perimetro");
		var modal = document.querySelector("#conteudo-perimetro");

		document.getElementById("conteudo-perimetro").innerHTML = "";

		var lado1 = parseFloat(form.lado1.value);
		var lado2 = parseFloat(form.lado2.value);
		var lado3 = parseFloat(form.lado3.value);

		var resultado = document.createElement("p");

		resultado.appendChild(perimetroTriangulo(lado1, lado2, lado3));

		modal.appendChild(resultado);
	});

</script>