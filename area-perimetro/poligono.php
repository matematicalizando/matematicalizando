<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Polígono', 'poligono.png', 36, 36);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                       Um polígono diz-se regular se tiver todos os seus lados e ângulos iguais, sejam eles internos ou externos. Todo polígono regular pode ser inscrito em uma circunferência. <span><a href="https://pt.wikipedia.org/wiki/Pol%C3%ADgono_regular" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#area">Área</a></li>
                        <li><a data-toggle="tab" href="#perimetroPoligono">Perímetro</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="area" class="tab-pane fade in active">
                            <form id="form-area">
                                <div class="tab-ctn">
                                    <p>A área do Polígono é dada pela fórmula: <strong>A = (P / 2) * a</strong>.</p>
                                    <p>Legenda: <strong>A = </strong>Área, <strong>P = </strong>Perímetro, <strong>a = </strong>Apótema</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Perímetro', 'perimetro');
                                            $funcoes->MontaInputNumPositivo('Apótema', 'apotema');
                                            $funcoes->MontaModal('area');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>

                        <div id="perimetroPoligono" class="tab-pane fade">
                            <form id="form-perimetro">
                                <div class="tab-ctn">
                                    <p>O perímetro do Polígono regular é dado pela fórmula: <strong>P = lado * n</strong>.</p>
                                    <p>Legenda: <strong>P = </strong>Perímetro, <strong>n = </strong>Número de Lados</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Lado', 'lado');
                                            $funcoes->MontaInputNumPositivo('Número de Lados', 'nLados');
                                            $funcoes->MontaModal('perimetro');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularArea = document.querySelector("#botao-calcular-area");
    botaoCalcularArea.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-area");
        var modal = document.querySelector("#conteudo-area");

        document.getElementById("conteudo-area").innerHTML = "";

        var perimetro = parseFloat(form.perimetro.value);
        var apotema = parseFloat(form.apotema.value);

        var resultado = document.createElement("p");

        resultado.appendChild(areaPoligono(perimetro, apotema));

        modal.appendChild(resultado);
    });

    var botaoCalcularPerimetro = document.querySelector("#botao-calcular-perimetro");
    botaoCalcularPerimetro.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-perimetro");
        var modal = document.querySelector("#conteudo-perimetro");

        document.getElementById("conteudo-perimetro").innerHTML = "";

        var lado = parseFloat(form.lado.value);
        var nLados = parseFloat(form.nLados.value);

        var resultado = document.createElement("p");

        resultado.appendChild(perimetroPoligono(lado, nLados));

        modal.appendChild(resultado);
    });

</script>