<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Círculo', 'circulo.png', 36, 36);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                      Na geometria, um círculo ou disco é o conjunto dos pontos internos de uma circunferência. Por vezes, também se chama círculo o conjunto de pontos cuja distância ao centro é menor ou igual a um dado valor (ao qual chamamos raio). <span><a href="https://pt.wikipedia.org/wiki/C%C3%ADrculo" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#area">Área</a></li>
                        <li><a data-toggle="tab" href="#perimetro">Perímetro</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="area" class="tab-pane fade in active">
                            <form id="form-area">
                                <div class="tab-ctn">
                                    <p>A área do Círculo é dada pela fórmula: <strong>A = π * r²</strong>.</p>
                                    <p>Legenda: <strong>A = </strong>Área, <strong>π = </strong>Pi, <strong>r = </strong>Raio</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Raio', 'raio');
                                            $funcoes->MontaModal('area');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>

                        <div id="perimetro" class="tab-pane fade">
                            <form id="form-perimetro">
                                <div class="tab-ctn">
                                    <p>O perímetro do Círculo é dado pela fórmula: <strong>P = 2 * π * r</strong>.</p>
                                    <p>Legenda: <strong>P = </strong>Perímetro, <strong>π = </strong>Pi, <strong>r = </strong>Raio</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Raio', 'raio');
                                            $funcoes->MontaModal('perimetro');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularArea = document.querySelector("#botao-calcular-area");
    botaoCalcularArea.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-area");
        var modal = document.querySelector("#conteudo-area");

        document.getElementById("conteudo-area").innerHTML = "";

        var raio = parseFloat(form.raio.value);

        var resultado = document.createElement("p");

        resultado.appendChild(areaCirculo(raio));

        modal.appendChild(resultado);
    });

    var botaoCalcularPerimetro = document.querySelector("#botao-calcular-perimetro");
    botaoCalcularPerimetro.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-perimetro");
        var modal = document.querySelector("#conteudo-perimetro");

        document.getElementById("conteudo-perimetro").innerHTML = "";

        var raio = parseFloat(form.raio.value);
       
        var resultado = document.createElement("p");

        resultado.appendChild(perimetroCirculo(raio));

        modal.appendChild(resultado);
    });

</script>