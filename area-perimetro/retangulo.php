<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Retângulo', 'retangulo.png', 50, 36);
?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="widget-tabs-int">
				<div class="tab-hd">
					<p>
						Um retângulo é um quadrilátero que possui todos os ângulos internos congruentes. Assim percebemos que todo retângulo é também um paralelogramo, cujos ângulos internos são ângulos retos. O quadrado é um caso particular de um retângulo em que todos os lados têm o mesmo comprimento.<br>
						A soma dos ângulos internos de um retângulo é 360°. <span><a href="https://pt.wikipedia.org/wiki/Ret%C3%A2ngulo" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
					</p>
				</div>
				<div class="widget-tabs-list">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#area">Área</a></li>
						<li><a data-toggle="tab" href="#perimetro">Perímetro</a></li>
					</ul>									
					<div class="tab-content tab-custom-st">
						<div id="area" class="tab-pane fade in active">
							<form id="form-area">
								<div class="tab-ctn">
									<p>A área do retângulo é dada pela fórmula: <strong>A = a * b</strong>.</p>
									<p>Legenda: <strong>A = </strong> Área, <strong>a = </strong> Altura, <strong>b = </strong>Base</p>
									<hr>
									<p class="tab-mg-b-0">
										<div align="center"><h4>Calcular</h4></div>
										<?php 
											$funcoes->MontaInputNumPositivo('Base', 'base'); 
											$funcoes->MontaInputNumPositivo('Altura', 'altura');
											$funcoes->MontaModal('area');
										?>
									</p>
								</div>
							</form>
						</div>

						<div id="perimetro" class="tab-pane fade">
							<form id="form-perimetro">
								<div class="tab-ctn">
									<p>O perímetro do retângulo é dado pela fórmula: <strong>P = 2 * (b + a)</strong>.</p>
									<p>Legenda: <strong>P = </strong> Perímetro, <strong>a = </strong> Altura, <strong>b = </strong>Base</p>
									<hr>
									<p class="tab-mg-b-0">
										<div align="center"><h4>Calcular</h4></div>
										<?php 
											$funcoes->MontaInputNumPositivo('Base', 'base'); 
											$funcoes->MontaInputNumPositivo('Altura', 'altura');
											$funcoes->MontaModal('perimetro');
										?>
									</p>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

	var botaoCalcularArea = document.querySelector("#botao-calcular-area");
	botaoCalcularArea.addEventListener("click", function(event){

		event.preventDefault();

		var form = document.querySelector("#form-area");
		var modal = document.querySelector("#conteudo-area");

		document.getElementById("conteudo-area").innerHTML = "";

		var base = parseFloat(form.base.value);
		var altura = parseFloat(form.altura.value);

		var resultado = document.createElement("p");

		resultado.appendChild(areaRetangulo(base, altura));

		modal.appendChild(resultado);
	});

	var botaoCalcularPerimetro = document.querySelector("#botao-calcular-perimetro");
	botaoCalcularPerimetro.addEventListener("click", function(event){

		event.preventDefault();

		var form = document.querySelector("#form-perimetro");
		var modal = document.querySelector("#conteudo-perimetro");

		document.getElementById("conteudo-perimetro").innerHTML = "";

		var base = parseFloat(form.base.value);
		var altura = parseFloat(form.altura.value);

		var resultado = document.createElement("p");

		resultado.appendChild(perimetroRetangulo(base, altura));

		modal.appendChild(resultado);
	});

</script>