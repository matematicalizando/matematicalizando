<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Trapézio', 'trapezio.png', 45, 45);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                       Na geometria, o trapézio é um quadrilátero com dois lados paralelos entre si, que são chamados de base maior e base menor. <span><a href="https://pt.wikipedia.org/wiki/Losango" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#area">Área</a></li>
                        <li><a data-toggle="tab" href="#perimetro">Perímetro</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="area" class="tab-pane fade in active">
                            <form id="form-area">
                                <div class="tab-ctn">
                                    <p>A área do Trapézio é dada pela fórmula: <strong>A = ((B * b) / 2) * h</strong>.</p>
                                    <p>Legenda: <strong>A = </strong>Área, <strong>B = </strong>Base maior, <strong>b = </strong>Base menor, <strong>h = </strong> Altura</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Base Maior', 'baseM');
                                            $funcoes->MontaInputNumPositivo('Base Menor', 'basem');
                                            $funcoes->MontaInputNumPositivo('Altura', 'altura');
                                            $funcoes->MontaModal('area');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>

                        <div id="perimetro" class="tab-pane fade">
                            <form id="form-perimetro">
                                <div class="tab-ctn">
                                    <p>O perímetro do Trapézio é dado pela fórmula: <strong>P = B + b + lado1 + lado2</strong>.</p>
                                    <p>Legenda: <strong>P = </strong>Perímetro, <strong>B = </strong>Base maior, <strong>b = </strong>Base menor</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Base Maior', 'baseM');
                                            $funcoes->MontaInputNumPositivo('Base Menor', 'basem');
                                            $funcoes->MontaInputNumPositivo('Lado 1', 'lado1');
                                            $funcoes->MontaInputNumPositivo('Lado 2', 'lado2'); 
                                            $funcoes->MontaModal('perimetro');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularArea = document.querySelector("#botao-calcular-area");
    botaoCalcularArea.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-area");
        var modal = document.querySelector("#conteudo-area");

        document.getElementById("conteudo-area").innerHTML = "";

        var baseM = parseFloat(form.baseM.value);
        var basem = parseFloat(form.basem.value);
        var altura = parseFloat(form.altura.value);

        var resultado = document.createElement("p");

        resultado.appendChild(areaTrapezio(baseM, basem, altura));

        modal.appendChild(resultado);
    });

    var botaoCalcularPerimetro = document.querySelector("#botao-calcular-perimetro");
    botaoCalcularPerimetro.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-perimetro");
        var modal = document.querySelector("#conteudo-perimetro");

        document.getElementById("conteudo-perimetro").innerHTML = "";

        var baseM = parseFloat(form.baseM.value);
        var basem = parseFloat(form.basem.value);
        var lado1 = parseFloat(form.lado1.value);
        var lado2 = parseFloat(form.lado2.value);

        var resultado = document.createElement("p");

        resultado.appendChild(perimetroTrapezio(baseM, basem, lado1, lado2));

        modal.appendChild(resultado);
    });

</script>