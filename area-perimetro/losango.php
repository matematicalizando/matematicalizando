<?php 
include '../moldes/moldeSuperior.php';

echo $funcoes->MontaCabecalho('Losango', 'losango.png', 36, 36);
?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-tabs-int">
                <div class="tab-hd">
                    <p>
                       Losango é um quadrilátero equilátero, ou seja, é um polígono formado por quatro lados de igual comprimento. Um losango é também um paralelogramo. Alguns autores exigem ainda que nenhum dos ângulos do quadrilátero seja reto para que ele seja considerado um losango. <span><a href="https://pt.wikipedia.org/wiki/Losango" target="_blank"> Saiba mais <i class="fas fa-info-circle" title="Clique aqui para saber mais sobre o assunto!"></i></a></span>
                    </p>
                </div>
                <div class="widget-tabs-list">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#area">Área</a></li>
                        <li><a data-toggle="tab" href="#perimetro">Perímetro</a></li>
                    </ul>                                   
                    <div class="tab-content tab-custom-st">
                        <div id="area" class="tab-pane fade in active">
                            <form id="form-area">
                                <div class="tab-ctn">
                                    <p>A área do losango é dada pela fórmula: <strong>A = (D * d) / 2</strong>.</p>
                                    <p>Legenda: <strong>A = </strong> Área, <strong>D = </strong> Diagonal maior, <strong>d = </strong>Diagonal menor</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Diagonal Maior', 'diagonalM');
                                            $funcoes->MontaInputNumPositivo('Diagonal Menor', 'diagonalm');
                                            $funcoes->MontaModal('area');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>

                        <div id="perimetro" class="tab-pane fade">
                            <form id="form-perimetro">
                                <div class="tab-ctn">
                                    <p>O perímetro do Losango é dado pela fórmula: <strong>P = lado * 4</strong>.</p>
                                    <p>Legenda: <strong>P = </strong> Perímetro</p>
                                    <hr>
                                    <p class="tab-mg-b-0">
                                        <div align="center"><h4>Calcular</h4></div>
                                        <?php 
                                            $funcoes->MontaInputNumPositivo('Lado', 'lado'); 
                                            $funcoes->MontaModal('perimetro');
                                        ?>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
include '../moldes/moldeInferior.php';
?>
<script>

    var botaoCalcularArea = document.querySelector("#botao-calcular-area");
    botaoCalcularArea.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-area");
        var modal = document.querySelector("#conteudo-area");

        document.getElementById("conteudo-area").innerHTML = "";

        var diagonalM = parseFloat(form.diagonalM.value);
        var diagonalm = parseFloat(form.diagonalm.value);

        var resultado = document.createElement("p");

        resultado.appendChild(areaLosango(diagonalM, diagonalm));

        modal.appendChild(resultado);
    });

    var botaoCalcularPerimetro = document.querySelector("#botao-calcular-perimetro");
    botaoCalcularPerimetro.addEventListener("click", function(event){

        event.preventDefault();

        var form = document.querySelector("#form-perimetro");
        var modal = document.querySelector("#conteudo-perimetro");

        document.getElementById("conteudo-perimetro").innerHTML = "";

        var lado = parseFloat(form.lado.value);

        var resultado = document.createElement("p");

        resultado.appendChild(perimetroLosango(lado));

        modal.appendChild(resultado);
    });

</script>